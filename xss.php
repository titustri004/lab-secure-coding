<html>
    <head>
        <title>XSS</title>
    </head>
    <body>
        <h1>Cross Site Scripting</h1>
        <a href="index.php">Back</a>
        <form action="xss.php" method="POST">
            <br>
            <label for="nama">Tuliskan namamu !</label><br>
            <input type="text" id="nama" name="nama"><br>
            <submit>
            <input type="submit" value="kirim" name="submit" id="submit">
            </submit>
        </form>

        <!-- Tidak ada validasi dan sanitasi -->
        <?php
            // Is there any input?
            if( array_key_exists( "nama", $_POST ) && $_POST[ 'nama' ] != NULL ) {
                // Feedback for end user
                echo $_POST['nama'];
            }
        ?> 
        <br>

        <!-- Input Validasi -->
        <?php
            // Is there any input?
            if( array_key_exists( "nama", $_POST ) && $_POST[ 'nama' ] != NULL ) {
                // Feedback for end user
                $name = $_POST ["nama"];  
                if (!preg_match ("/^[a-zA-z]*$/", $name) ) {  
                    $ErrMsg = "Only alphabets and whitespace are allowed.";          
                    echo $ErrMsg;  
                } else { 
                    echo $name;  
                }
                //echo $_POST['nama'];
            }
        ?> 
        <br>

        <!-- PHP strip_tags() -->
        <?php
            // Is there any input?
            if( array_key_exists( "nama", $_POST ) && $_POST[ 'nama' ] != NULL ) {
                // Feedback for end user
                echo strip_tags($_POST['nama']);
            }
        ?> 
        <br>       

        <!-- PHP htmlspecialchars() -->
        <?php
            // Is there any input?
            if( array_key_exists( "nama", $_POST ) && $_POST[ 'nama' ] != NULL ) {
                // Feedback for end user
                echo htmlspecialchars($_POST['nama']);
            }
        ?> 
        <br>
        
        <!-- PHP htmlentities() -->
        <?php
            // Is there any input?
            if( array_key_exists( "nama", $_POST ) && $_POST[ 'nama' ] != NULL ) {
                // Feedback for end user
                echo htmlentities($_POST['nama']);
            }
        ?> 
        <br>

        <!-- PHP Filter Sanitize -->
        <?php
            // Is there any input?
            if( array_key_exists( "nama", $_POST ) && $_POST[ 'nama' ] != NULL ) {
                // Feedback for end user
                $newstr = filter_var($_POST['nama'], FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
                echo $newstr;
            }
        ?>
    </body>
</html>