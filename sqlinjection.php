<?php
    $db_host = 'localhost';
    $db_name = 'lab';
    $db_user = 'root';
    $db_pass = '';
    $mysqli = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

?>
<html>
    <head><title>SQL Injection</title></head>
    <body>
    <h1>SQL Injection</h1>
    <a href="index.php">Back</a>
    <table width='20%' border=1>
    <tr>
        <th>ID</th><th>Nama</th>
    </tr>
    <?php
    $result2 = mysqli_query($mysqli, "SELECT * FROM data_manusia ORDER BY id DESC");
        while($data = mysqli_fetch_array($result2)) {
            echo "<tr>";
            echo "<td>".$data['id']."</td>";
            echo "<td>".$data['nama']."</td>";
        }
    ?>
    </table><br><br>

    <h3>Detail data</h3>
    <form action="#" method=POST>
        <input type="text" name="id">
        <input type="submit" value="Search ID">
    </form>
    <table width='50%' border=1>
    <tr>
        <th>ID</th><th>Nama</th><th>Tanggal Lahir</th><th>Umur</th>
    </tr>
    <?php
    if( array_key_exists( "id", $_POST ) && $_POST[ 'id' ] != NULL ) {
        // Feedback for end user
        $id = $_POST["id"];
        $result1 = mysqli_query($mysqli, "SELECT * FROM data_manusia WHERE id=$id ORDER BY id DESC LIMIT 2");
        while($data = mysqli_fetch_array($result1)) {
            echo "<tr>";
            echo "<td>".$data['id']."</td>";
            echo "<td>".$data['nama']."</td>";
            echo "<td>".$data['tgl_lahir']."</td>";
            echo "<td>".$data['umur']."</td>";
        };
    }
    
    ?>
    </table>
    
    </body>
</html>