<html>
    <head>
        <title>Lab</title>
    </head>
    <body>
        <h1>List Vulnerability</h1>
        <li><a href="xss.php">Cross Site Scripting</a></li>
        <li><a href="sqlinjection.php">SQL Injection</a></li>
        <li><a href="sqlinjection-secure.php">SQL Injection Secure</a></li>
        <li><a href="csrf.php">CSRF - Cross Site Request Forgery</a></li>
    </body>
</html>